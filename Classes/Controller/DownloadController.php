<?php
namespace MaikHagenbruch\Downloadcounter\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2017
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * DownloadController
 */
class DownloadController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     * @inject
     */
    protected $configurationManager;

    /**
     * downloadRepository
     *
     * @var \MaikHagenbruch\Downloadcounter\Domain\Repository\DownloadRepository
     * @inject
     */
    protected $downloadRepository = NULL;

    /**
     * pageRepository
     *
     * @var \TYPO3\CMS\Frontend\Page\PageRepository
     * @inject
     */
    protected $pageRepository = NULL;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $downloads = $this->downloadRepository->findAll();
        $this->view->assign('downloads', $downloads);
    }

    /**
     * action show
     *
     * @param \MaikHagenbruch\Downloadcounter\Domain\Model\Download $download
     * @return void
     */
    public function showAction(\MaikHagenbruch\Downloadcounter\Domain\Model\Download $download)
    {
        $downloads = $this->downloadRepository->findAllByName($download->getName());
        $this->view->assign('downloads', $downloads);
    }

    /**
     * Saves entry in DB
     *
     * @return void
     */
    public function saveAction()
    {
        $file = basename($_GET['file']);
        $date = new \DateTime;
        $pageId = $GLOBALS['TSFE']->id;
        $newDownload = new \MaikHagenbruch\Downloadcounter\Domain\Model\Download();
        $newDownload->setDate($date);
        $newDownload->setName($file);
        $newDownload->setPid($pageId);
        $newDownload->setPageTitle($this->getPageTitle($pageId));
        $this->downloadRepository->add($newDownload);
        return true;
    }

    /**
     * gets the page title from page id
     *
     * @param $pageId
     */
    public function getPageTitle($pageId)
    {
        $pageSelect = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Frontend\Page\PageRepository');
        $pageSelect->init(false);
        $page = $pageSelect->getPage($pageId);
        return $page['title'];
    }

    public function exportAction()
    {
        $downloadName = $this->request->getArgument('name');
        $downloads = $this->downloadRepository->findAllByName($downloadName);
        $list = ["Dateiname,Download auf Seite,Download am"];
        foreach($downloads as $download) {
            //DebuggerUtility::var_dump($download);die();
            array_push($list, $download->getName().",".$download->getPageTitle().",".date('d.m.y H:m:s', $download->getDate()->getTimestamp()));
        }
        //DebuggerUtility::var_dump($downloads);die();
        $file = fopen('php://memory', 'w');
        foreach($list as $line) {
            fputcsv($file, explode(',',$line));
        }
        fseek($file, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename=download.csv');
        fpassthru($file);
    }
}