.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt


.. _links:

Links
-----

:TER:
	https://typo3.org/extensions/repository/view/downloadcounter

:Bug Tracker:
	https://bitbucket.org/mhagenbruch/downloadcounter/issues

:Git Repository:
	https://bitbucket.org/mhagenbruch/downloadcounter

:Contact:
	maik@hagenbruch.info
