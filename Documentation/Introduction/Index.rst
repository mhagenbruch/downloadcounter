﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _introduction:

Introduction
============


.. _what-it-does:

What does it do?
----------------

This little helper allows backend users to view the amount of downloaded pdf files.
There are a list View with amounts, and a detail view with date and time and page name from which it was loaded.


.. _screenshots:

Screenshots
-----------

This chapter should help people figure how the extension works. Remove it
if not relevant.

.. figure:: ../Images/IntroductionPackage.png
   :width: 500px
   :alt: Introduction Package

   Introduction Package just after installation (caption of the image)

   How the Frontend of the Introduction Package looks like just after installation (legend of the image)
