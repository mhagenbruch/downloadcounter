﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _admin-manual:

Administrator Manual
====================


.. _admin-installation:

Installation
------------

Installation is very simple. Install it via extension manager and include the typoscript template into your root template
and you are done.


.. _admin-configuration:

Configuration
-------------

There are no configuration options for now.
If you have wishes, please let me now.
