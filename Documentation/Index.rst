﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
downloadcounter
=============================================================

.. only:: html

	:Classification:
		downloadcounter

	:Version:
		|alpha|

	:Language:
		en

	:Description:
		This little extension creates a backendmodule. This module shows a list of clicked pdf (for now only) links.

	:Keywords:
		downloadcounter, clickcounter, 

	:Copyright:
		2017

	:Author:
		Maik Hagenbruch

	:Email:
		maik@hagenbruch.info
		
	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 3
	:titlesonly:

	Introduction/Index
	User/Index
	Administrator/Index
	How/Index
	ChangeLog/Index
	Links
