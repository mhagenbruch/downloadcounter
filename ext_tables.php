<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
	'MaikHagenbruch.' . $_EXTKEY,
	'Ajax',
	'ajax'
);

if (TYPO3_MODE === 'BE') {

	/**
	 * Registers a Backend Module
	 */
	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
		'MaikHagenbruch.' . $_EXTKEY,
		'web',	 // Make module a submodule of 'user'
		'downloadlist',	// Submodule key
		'',						// Position
		array(
			'Download' => 'list, show, export',
		),
		array(
			//'access' => 'user,group',
			//'icon'   => 'EXT:' . $_EXTKEY . '/ext_icon.gif',
			'labels' => 'LLL:EXT:' . $_EXTKEY . '/Resources/Private/Language/locallang_mod.xml',
		)
	);

}

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'downloadcounter');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_downloadcounter_domain_model_download', 'EXT:downloadcounter/Resources/Private/Language/locallang_csh_tx_downloadcounter_domain_model_download.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_downloadcounter_domain_model_download');
