<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'MaikHagenbruch.' . $_EXTKEY,
	'Ajax',
	array(
		'Download' => 'save, export',
		
	),
	// non-cacheable actions
	array(
		'Download' => '',
		
	)
);
