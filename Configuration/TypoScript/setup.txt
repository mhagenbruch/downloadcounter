
plugin.tx_downloadcounter_ajax {
	view {
		templateRootPaths.0 = {$plugin.tx_downloadcounter_ajax.view.templateRootPath}
		partialRootPaths.0 = {$plugin.tx_downloadcounter_ajax.view.partialRootPath}
		layoutRootPaths.0 = {$plugin.tx_downloadcounter_ajax.view.layoutRootPath}
	}
	persistence {
		storagePid = {$plugin.tx_downloadcounter_ajax.persistence.storagePid}
	}
}

plugin.tx_downloadcounter._CSS_DEFAULT_STYLE (
	textarea.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	input.f3-form-error {
		background-color:#FF9F9F;
		border: 1px #FF0000 solid;
	}

	.tx-downloadcounter table {
		border-collapse:separate;
		border-spacing:10px;
	}

	.tx-downloadcounter table th {
		font-weight:bold;
	}

	.tx-downloadcounter table td {
		vertical-align:top;
	}

	.typo3-messages .message-error {
		color:red;
	}

	.typo3-messages .message-ok {
		color:green;
	}

)

 # Module configuration
module.tx_downloadcounter {
	settings < plugin.tx_downloadcounter_ajax.settings
	persistence < plugin.tx_downloadcounter_ajax.persistence
	view < plugin.tx_downloadcounter_ajax.view
	view {
		templateRootPaths.0 = EXT:downloadcounter/Resources/Private/Backend/Templates/
		partialRootPaths.0 = EXT:downloadcounter/Resources/Private/Backend/Partials/
		layoutRootPaths.0 = EXT:downloadcounter/Resources/Private/Backend/Layouts/
	}
}
page.includeJSFooter.tx_downloadcounter = EXT:downloadcounter/Resources/Public/JavaScript/counter.js
ajaxpage = PAGE
ajaxpage {
	typeNum = 99999
	10 < tt_content.list.20.downloadcounter_ajax
	config {
		disableAllHeaderCode = 1
		additionalHeaders = Content-type:application/json
		xhtml_cleaning = 0
		admPanel = 0
		debug = 0
		no_cache = 1
	}
}
